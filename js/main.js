Array.prototype.move = function (from, to) {
    this.splice(to, 0, this.splice(from, 1)[0])
}

Array.prototype.remove = function (from) {
    return this.splice(from, 1)[0]
}

Array.prototype.addTo = function (element, to) {
    this.splice(to, 0, element)
}

Array.prototype.jsonClone = function () {
    return JSON.parse(JSON.stringify(this));
};

Object.prototype.jsonClone = function () {
    return JSON.parse(JSON.stringify(this));
};

const GRUPO = [
    'Retornavel',
    'Descartavel',
    'Alugavel'
]

const SUBGRUPO = [
    'Aluminio',
    'Bag',
    'BIB',
    'Chopp',
    'Copo',
    'Garrafa',
    'Lata',
    'PET',
    'PVC',
    'Tetrapak'
]

const SUBTIPO = [
    'Agua',
    'BebidaAlcolicaAro',
    'Cerveja',
    'Cha',
    'Energetico',
    'Isotonico',
    'Refrigerante',
    'Suco'
]

const JsonManipulation = (() => {
    const buttonLoad = document.getElementById("button-json-to-tables")
    const buttonUndo = document.getElementById("button-undo")
    const textAreaJson = document.getElementById("json-input")
    const textAreaResult = document.getElementById("json-result")
    const jsonTablesHtml = document.querySelector("[jsonTables]")
    const tablesToJsonForm = document.getElementById("tables-to-json")

    let stringJsonInput
    let stringJsonOutput
    let palletList
    let palletListOriginal
    let jsonData
    let jsonDataOriginal
    let jsonDataFinal
    const maxBackups = 5
    let currentBackup = 0
    let jsonDataLastChanges = []

    const getJsonString = () => {
        stringJsonInput = textAreaJson.value
    }
    
    const publishJsonString = () => {
        textAreaResult.value = stringJsonOutput
    }

    const prepareFinalJson = () => {
        jsonDataFinal = jsonData.jsonClone()
        let pallets
        if (jsonDataFinal[0]) {
            pallets = jsonDataFinal[0].pallets
        } else {
            pallets = jsonDataFinal.pallets
        }

        for (let palletIndex = 0; palletIndex < pallets.length; palletIndex++) {
            const pallet = pallets[palletIndex].items
            for (let itemIndex = 0; itemIndex < pallet.length; itemIndex++) {
                const itemVerified = pallet[itemIndex]
                let foundedItems = pallet.filter(item => item.code === itemVerified.code)
                if(foundedItems.length > 1){
                    const total = foundedItems.reduce((a, b) => {
                        return parseFloat(a.quantity) + parseFloat(b.quantity)
                    })
                    foundedItems[0].quantity = total.toString()
                    pallet.remove(pallet.findIndex(item => item.code === foundedItems[0].code && item.quantity != foundedItems[0].quantity))
                }
            }
        }
        stringJsonOutput = JSON.stringify(jsonDataFinal)
        publishJsonString()
    }

    const backupData = () => {
        const olderBackupId = jsonDataLastChanges.length - 1
        if (olderBackupId >= maxBackups - 1) {
            jsonDataLastChanges.remove(olderBackupId)
        }
        jsonDataLastChanges.addTo(jsonData.jsonClone(), 0)
    }

    const undoRepositioning = () => {
        if (jsonDataLastChanges.length >= 1) {
            jsonData = jsonDataLastChanges[currentBackup].jsonClone()
            currentBackup++
        }
    }

    const applyItemColor = () => {
        let palletElementId = 1
        palletList.forEach(pallet => {
            if (pallet.length > 0) {
                let itemIndex = 0
                const palletItems = pallet.items
                palletItems.forEach(item => {
                    const itemElement = document.getElementById(`pallet-${palletElementId}`).children.item(itemIndex)
                    const currentTrainApply = item.train_apply.toUpperCase()
                    if (item.focus != "0") {
                        switch (currentTrainApply) {
                            case "DISTRIBUTION":
                                itemElement.classList.add("has-background-primary")
                                itemElement.classList.remove("has-background-info")
                                break;

                            case "SORT":
                                itemElement.classList.add("has-background-info")
                                itemElement.classList.remove("has-background-primary")
                                break;

                            default:
                                break;
                        }
                    } else {
                        itemElement.classList.remove("has-background-info")
                        itemElement.classList.remove("has-background-primary")
                    }
                    itemIndex++
                })
            }
            palletElementId++
        })
    }

    const updatePalletOcupation = () => {
        let palletIndex = 1

        palletList.forEach(pallet => {
            const items = pallet.items
            let occupation = 0
            items.forEach(item => {
                const quantity_pallet = item.quantity_pallet
                const quantity = item.quantity
                occupation += ((quantity * 42 ) / quantity_pallet)
            })
            const occupationElement = document.querySelector(`[current-occupation-${palletIndex}]`)
            occupationElement.innerHTML = `<B>${occupation.toFixed(0)}</B>`
            if (occupation > 42) {
                occupationElement.classList.remove('is-primary')
                occupationElement.classList.add('is-danger')
            } else {
                occupationElement.classList.remove('is-danger')
                occupationElement.classList.add('is-primary')
            }
            palletIndex++
        })
    }

    const verifyOriginalItemOrder = destinyPalletId => {
        const jsonDataItems = palletList[destinyPalletId].items
        const jsonDataOriginalItems = palletListOriginal[destinyPalletId].items
        const lowerLength = Math.min(jsonDataItems.length, jsonDataOriginalItems.length)

        for (let index = 0; index < lowerLength; index++) {
            const item = jsonDataItems[index]
            const originalItem = jsonDataOriginalItems[index];
            if (originalItem.focus === "0") {
                if (item.code === originalItem.code && item.quantity === originalItem.quantity) {
                    item.focus = originalItem.focus
                    item.train_apply = originalItem.train_apply
                }
            }
        }
    }

    const verifySameItemOnPallet = (destinyPalletId, itemNewIndex) => {
        const jsonDataItems = palletList[destinyPalletId].items
        const item = jsonDataItems[itemNewIndex]
        let totalDuplicatedItems = 0

        for (let index = 0; index < jsonDataItems.length; index++) {
            const itemVerified = jsonDataItems[index];
            if (itemNewIndex != index && item.code === itemVerified.code) {
                totalDuplicatedItems++
                itemVerified.focus = "1"
                itemVerified.train_apply = "Sort"
            }
        }
        if (totalDuplicatedItems > 0) {
            item.focus = "1"
            item.train_apply = "Sort"
        }
    }


    const getItemIfBelongsOriginalPallet = (destinyPalletId, itemNewIndex) => {
        const jsonDataItems = palletList[destinyPalletId].items
        const jsonDataOriginalItems = palletListOriginal[destinyPalletId].items

        const item = jsonDataItems[itemNewIndex]

        for (let index = 0; index < jsonDataOriginalItems.length; index++) {
            const originalItem = jsonDataOriginalItems[index];
            if (item.code === originalItem.code && item.quantity === originalItem.quantity) {
                return item
            }
        }
        return null
    }

    const onPalletReorder = (evt) => {
        const originPalletId = evt.oldIndex
        const destinyPalletId = evt.newIndex
    }

    const reorderJsonSamePallet = (destinyPalletId, itemOldIndex, itemNewIndex, itemElement) => {
        const jsonItems = palletList[destinyPalletId].items
        jsonItems.move(itemOldIndex, itemNewIndex)
    }

    const reorderJsonDifferentPallet = (originPalletId, destinyPalletId, itemOldIndex, itemNewIndex) => {
        const jsonOriginPalletItems = palletList[originPalletId].items
        const jsonDestinyPalletItems = palletList[destinyPalletId].items

        const removedItem = jsonOriginPalletItems.remove(itemOldIndex)
        jsonDestinyPalletItems.addTo(removedItem, itemNewIndex)
        const item = jsonDestinyPalletItems[itemNewIndex]

        item.focus = "1"
        item.train_apply = "Distribution"
    }

    const onItemReorder = (evt) => {
        const originPalletElementId = evt.from.id
        const originPalletId = originPalletElementId.replace('pallet-', '') - 1
        const destinyPalletElementId = evt.to.id
        const destinyPalletId = destinyPalletElementId.replace('pallet-', '') - 1
        const itemOldIndex = evt.oldIndex
        const itemNewIndex = evt.newIndex
        const itemElement = evt.item

        backupData()
        currentBackup = 0

        if (originPalletElementId === destinyPalletElementId) {
            reorderJsonSamePallet(destinyPalletId, itemOldIndex, itemNewIndex, itemElement)
        } else {
            reorderJsonDifferentPallet(originPalletId, destinyPalletId, itemOldIndex, itemNewIndex)
            verifyOriginalItemOrder(originPalletId)
        }

        const item = getItemIfBelongsOriginalPallet(destinyPalletId, itemNewIndex)
        if (item) {
            item.focus = "1"
            item.train_apply = "Sort"
        }
        verifyOriginalItemOrder(destinyPalletId)
        verifySameItemOnPallet(destinyPalletId, itemNewIndex)
        updatePalletOcupation()
        applyItemColor()
        prepareFinalJson()
    }

    const applyDragAndDrop = totalPallets => {
        for (let index = 1; index < totalPallets; index++) {
            const element = document.getElementById(`pallet-${index}`)
            Sortable.create(element, {
                group: 'pallets',
                animation: 100,
                handle: '.drag-handle',
                onUpdate: function (evt) {
                    onItemReorder(evt)
                },
                onAdd: function (evt) {
                    onItemReorder(evt)
                }
            });
        }
        const element = document.querySelector("[jsontables]")
        Sortable.create(element, {
            animation: 100,
            handle: '.message-header',
            onUpdate: function (evt) {
                onPalletReorder(evt)
            }
        });
    }

    const loadJson = () => {
        getJsonString()
        jsonDataOriginal = JSON.parse(stringJsonInput)
        jsonData = jsonDataOriginal.jsonClone()
    }

    const loadPalletList = () => {
        if (jsonData[0]) {
            palletList = jsonData[0].pallets
            palletListOriginal = jsonDataOriginal[0].pallets
        } else {
            palletList = jsonData.pallets
            palletListOriginal = jsonDataOriginal.pallets
        }
    }

    const cleanChanges = () => {
        textAreaResult.value = ""
        jsonTablesHtml.innerHTML = ""
    }

    const getTextColor = (text) => {
        let result
        switch (text) {
            case "0":
                result = "text-white-border has-text-success"
                break;
            case "1":
                result = "text-white-border has-text-danger"
                break;
            case "2":
                result = "text-white-border has-text-link"
                break;
            case "3":
                result = "text-white-border has-text-primary"
                break;
            case "4":
                result = "text-white-border has-text-info"
                break;
            case "5":
                result = "text-white-border has-text-warning"
                break;
            case "6":
                result = "text-white-border has-text-grey-light"
                break;
            case "7":
                result = "text-white-border has-text-grey-dark"
                break;
            case "8":
                result = "text-white-border has-text-grey-darker"
                break;
            case "9":
                result = "text-white-border has-text-grey-lighter"
                break;
            default:
                result = ""
                break;
        }
        return result
    }

    const createTables = () => {
        const tableBeginCode = '<div class="table-pallet"><div class="table-header"><div class="columns has-background-grey has-text-white top-round-bordered"><div class="column is-025"></div><div class="column">group</div><div class="column">subgroup</div><div class="column">subtype</div><div class="column is-1">code</div><div class="column is-5">name</div><div class="column is-1">qtd_max</div><div class="column is-1">quantity</div></div></div>'
        const tableEndCode = '</div></div></div></article>'
        let palletIndex = 1

        palletList.forEach(pallet => {
            const palletId = `pallet-${palletIndex}`
            const htmlTableBody = `<div id="${palletId}" class="table-body">`
            const height = pallet.height
            const length = pallet.length
            const pallet_name = pallet.pallet
            const pallet_id = pallet.pallet_id
            const size = pallet.size
            const width = pallet.width
            const palletInfo = `<article id="article-${palletIndex}" class="message"><div class="message-header"><p>PALLET ${palletIndex}</p><a id="button-pallet-${palletIndex}" class="button is-dark is-small">recolher</a></div><div class="message-body inner-shadow "><div class="box level"><div class="level-left">pallet: ${pallet_name} | id: ${pallet_id} | length: ${length} | height: ${height} | width: ${width} | size: ${size}</div><div  class="level-right level"><div class="tags has-addons level-right"><span class="tag">Ocupação</span><span current-occupation-${palletIndex} class="tag is-primary">X.XX%</span></div></div></div>`

            jsonTablesHtml.innerHTML += palletInfo + tableBeginCode + htmlTableBody + tableEndCode

            const tbodyElement = document.getElementById(palletId)
            const rowItens = pallet.items

            rowItens.forEach(item => {
                const group = GRUPO[item.goup]
                const subgroup = SUBGRUPO[item.sub_goup]
                const subtype = SUBTIPO[item.sub_type]
                const code = item.code
                const name = item.name
                const quantity = item.quantity
                const quantity_pallet = item.quantity_pallet
                const groupTextColor = getTextColor(item.goup)
                const subgroupTextColor = getTextColor(item.sub_goup)
                const subtypeTextColor = getTextColor(item.sub_type)

                const htmlTableRowItens = `<div class="columns has-background-white bordered"><div class="column is-025 drag-handle">☰</div><div class="column ${groupTextColor}">${group}</div><div class="column ${subgroupTextColor}">${subgroup}</div><div class="column ${subtypeTextColor}">${subtype}</div><div class="column is-1">${code}</div><div class="column is-5">${name}</div><div class="column is-1">${quantity_pallet}</div><div class="column is-1">${quantity}</div></div>`
                tbodyElement.innerHTML += htmlTableRowItens
            })
            palletIndex++
        });
        textAreaJson.style.height = '100px'
        tablesToJsonForm.classList.remove('hidden')
        return palletIndex
    }

    const setMinimizeAction = totalPallets => {
        const buttons = document.querySelectorAll("article a")
        for (let index = 1; index < totalPallets; index++) {
            const button = buttons[index - 1]
            button.addEventListener("click", () => {
                const articlePallet = document.getElementById(`article-${index}`)
                const icon = document.getElementById(`button-pallet-${index}`)
                if (icon.innerHTML === 'recolher') {
                    icon.innerHTML = 'expandir'
                    articlePallet.children[1].classList.add('hidden')
                } else {
                    icon.innerHTML = 'recolher'
                    articlePallet.children[1].classList.remove('hidden')
                }
            })
        }
    }

    const load = fromBackup => {
        cleanChanges()
        if (fromBackup) {
            undoRepositioning()
        } else {
            loadJson()
        }
        loadPalletList()
        const totalPallets = createTables()
        updatePalletOcupation()
        applyDragAndDrop(totalPallets)
        setMinimizeAction(totalPallets)
        prepareFinalJson()
        applyItemColor()
    }

    const events = () => {
        buttonLoad.addEventListener("click", () => {
            const fromBackup = false
            load(fromBackup)
            backupData()
        })
        buttonUndo.addEventListener("click", () => {
            if (jsonDataLastChanges.length > currentBackup) {
                const fromBackup = true
                load(fromBackup)
            }
        })

        document.addEventListener("keydown", function (e) {
            if (e.ctrlKey && e.code === "KeyZ") {
                e.preventDefault()
                if (jsonDataLastChanges.length > currentBackup) {
                    const fromBackup = true
                    load(fromBackup)
                }
            }
        })
    }

    return {
        start: () => {
            events()
        },
        events: events
    }
})()
JsonManipulation.start()

const documentReady = () => {

}

if (document.readyState !== 'loading') {
    documentReady();
} else {
    document.addEventListener('DOMContentLoaded', documentReady);
}